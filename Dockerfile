FROM node:10 as build-back
WORKDIR /app
COPY . .
RUN rm -rf /app/node_modules
RUN npm install
RUN npm run tsc
RUN cd node_modules/duniter/neon/ && NODE_ENV=production ./build.sh

FROM node:9 as build-front
COPY --from=build-back /app /app
WORKDIR /app/ui
RUN npm install
RUN npm run postinstall

FROM node:10
COPY --from=build-front /app /app
WORKDIR /app
CMD ["node", "index.js", "remuniter"]
EXPOSE 8555
